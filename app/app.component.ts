import { Component } from '@angular/core';

@Component({
  selector: 'color-app',
  templateUrl: 'app/templates/color.component.html',
  styleUrls: [ 'app/css/style.css']
})
export class AppComponent {
  title = 'Markers';
  colors = ['red', 'green', 'blue'];
  addColor(newColor: string) {
    if (newColor) {
      this.colors.push(newColor)
    }
  }
    removeColor(index) {
      this.colors.splice(index, 1)
  }

}

